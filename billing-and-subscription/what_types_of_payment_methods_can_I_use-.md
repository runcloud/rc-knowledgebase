You may use any debit card that is linked to a bank account.

If you use a credit card directly through our service, you may use VISA and Mastercard.

Or you may use Paypal, which allows for all major credit cards including VISA, Mastercard, American Express, and Discover.
