When you subscribe to our service, you agree to pay for the service for the duration of your usage. If monthly, you will be billed for the month and if yearly, you will be billed for the year.

To ensure that your service does not renew, click on the cog wheel on the far upper right of the dashboard. Navigate and click on **subscription** and click on **disable Auto Recharge** and change your subscription to the Free Plan.
