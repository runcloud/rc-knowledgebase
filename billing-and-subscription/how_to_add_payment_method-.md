In the far upper right hand side, click on the cog wheel.

1. Navigate down to and click **Payment Method**.

You may add your credit card or debit card directly or you may choose Paypal.
