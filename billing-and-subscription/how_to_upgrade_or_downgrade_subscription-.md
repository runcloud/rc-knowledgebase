Click on the cog wheel on the top right hand side of your screen.

1. Next scroll down the menu on the left hand side and click **Subscription**.
2. Scroll down the page and you will see your Current plan.
3. On this screen, you may also enter any promo codes.
4. To change your plan, click on **Change Plan**.

You may choose from Free, Basic, Pro, or the Business plan.

On this page, you will also be able to compare each plan accordingly to see which is a match for you.

You may choose monthly or yearly and click on **Choose Plan**.

By choosing yearly, you will receive the biggest discounts.
