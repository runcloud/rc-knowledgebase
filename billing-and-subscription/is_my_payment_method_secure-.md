RunCloud uses Paypal as a third-party payment processor to charge your credit or debit card and does not keep any details about your payment information.
