On the far upper right of your screen, click on the cog wheel.

By default, **Profile** will be selected.

Scroll down to **Company Details** and fill in the accompanying fields.

This information will not be made public and is used for invoices and receipts.

Click **Update Company Details**.
