It is rare that your account will be banned by us, unless you are caught reverse-engineering and abusing our technology. However, your account may also be banned if you delete your account permanently.

If you decide you wish to be unbanned, you will need to contact Technical Support to re-activate your account.
