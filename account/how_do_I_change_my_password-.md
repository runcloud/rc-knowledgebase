On the upper far right of your screen, click the cog wheel.

1. Navigate and click **Authentication** on the menu.
2. You will need to enter in your current password and your new password twice.

<div class="alert alert-info">
    It is highly recommended that you use RunCloud's password generator to generate your password, with at least 32 characters.
</div>
