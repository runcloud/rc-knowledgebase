On the far upper right of your screen, click on the cog wheel.

By default, **Profile** will be selected.

Scroll down and select the time zone you are in.

Click **Update My Details**.
