On the far upper right hand side, click the cog wheel.

Navigate and click **Newsletter**.

From here, you will be able to select the type of subscriptions you wish to receive:

-   Updates and Announcements
-   Blog
-   Events and Meetups

Or you may unsubscribe from the newsletter completely.

Once you have selected your options, click **Update**.
