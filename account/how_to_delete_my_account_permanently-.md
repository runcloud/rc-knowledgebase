On the upper far right of your screen, click the cog wheel.

Navigate and click **Delete account** on the menu.

RunCloud wants to ensure you really mean to delete your account, so there is several things you will need to do in order to verify that you want to delete your account permanently.

1. Enter in your email address
2. Check off that you want to certify deletion of your account
3. Check off that you want to ban your account to prevent future registration
4. Check off that you understand RunCloud will keep your invoices and reciepts for future reference

Once you have confirmed everything, click **Delete My Account**.

While you may not be able to restore your account or any data on it, if you decide you wish to return to RunCloud at a later time using the same email address, you will need to contact Technical Support to re-activate your account.
