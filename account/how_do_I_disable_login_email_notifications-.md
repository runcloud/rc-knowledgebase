Every time you login to your account, you will be notified via email.

On the far upper right of your screen, click on the cog wheel.

By default, **Profile** will be selected.

Scroll down to **Login Email Notification** and unselect **Login Email Notification**.

Click **Update My Details**.
