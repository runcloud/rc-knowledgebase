#### Introduction

The ability to work on a project with multiple contributors has always been the way of Github. As RunCloud was mainly built by developers for developers, they have kept to the philosophy of being able to have multiple collaborators work on a project together. RunCloud encourages team collaboration through the use one of its primary features, Teams.

RunCloud Teams currently have three different preset options including System Administrator, Interns, and Vendors. All have their own set of privelages that give limited access to whoever is on the team. Once a team has been created, you will be taken back to this screen, where you can delete the team, add new members to the team, connect the team to a specific server, or edit the team permissions.

Upon logging in with the invited email address, access to certain features will be restricted. Not every team member has to have access to create or delete servers. If you'd like to give a just a few email addresses restricted privileges while making one or two other emails a system administrator, you can create an additional team and assign it to the same server.

Teams is likely to be useful for agencies who have multiple people working on projects or companies who wish to give their clients access without giving them full access. RunCloud Teams is only available on the Business plan, but you will not pay anything extra for adding multiple team members and you can add as many teams as you need.
