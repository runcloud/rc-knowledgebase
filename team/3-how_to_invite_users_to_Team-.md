Follow steps below:

1. At the very top, click on **Teams**.
2. Locate the Team you created and click on the **user** icon that says **Invite Users**.
3. Enter in their email address and click **Assign**.

This user will receive an email with a link to accept their new role on the team.
