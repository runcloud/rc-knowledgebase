You may create as many Teams as you wish. Within each Team, you may grant specific permissions and invite users. You may add their email to a Team with specific permissions.
