#### Introduction

Team permissions grant approved users to view and access specific areas they can control on your server, including adding or deleting web applications, viewing server health, changing security and firewall settings, etc.
