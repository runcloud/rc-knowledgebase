Follow steps below:

1. On the top of your dashboard, click on **Teams**.
2. Locate your Team and click on the **server** icon that says **Assign Team to Server**.
3. Click the circle plus (+) sign to grant this team permissions to a server.
