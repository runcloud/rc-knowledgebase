Follow steps below:

1. At the very top of your dashboard, click on **Teams**.
2. Click on **Create a Team**.
3. There are several predefined team permissions you may choose from including **Vendors**, **Interns**, and **System Administrators**, each with more permissions than the next.
4. You may also select from any of the predefined team permissions and select or unselect permissions.
5. Once you have selected the permissions your team should have, give your team a name and a description and click **Add Team**.
