On the **Referral** page, ensure that you have entered in your Paypal account.

For each referral who signs up and subscribes to a paid plan, you will receive \$5.

Minimum payout is \$30 and you may click the **Cash Out** button.
