The referral program can be found on the menu under **Referral**.

Copy and share the link provided which is your affiliate link.

Anyone who clicks this link will be registered through the system as being referred by you. Once they sign up for a paid plan, you will received \$5 and 5 free days of service.

Ensure your Paypal email is entered correctly and once you reach \$30 in referrals, you may request a payout by clicking on the **Cash Out** button.
