You may only transfer a server from account to account on RunCloud.

1. Navigate to **Settings** on the menu.
2. Scroll down to **Transfer Server**.
3. Enter in the email address that will receive this server.

<div class="alert alert-danger">
The Slack Webhook URL, Telegram Notification, and Health Notification settings will be removed and must be re-activated on the new server.
</div>

<div class="alert alert-danger">
Any backup instances will be deleted after the transfer is complete but will be moved to the Backup Archive.
</div>

<div class="alert alert-danger">
It is best to remove SSL before transferring the server and re-add it once the transfer is complete.
</div>

<div class="alert alert-danger">
If you have any Web Applications using Atomic Deployment, you need to remove these Web Applications from Atomic Deployment.
</div>

<div class="alert alert-danger">
This server is only transferred via RunCloud and not on your official VPS host.
</div>
