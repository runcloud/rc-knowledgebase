To increase backup space, select **Backup Space** on the menu.

Select your desired package which includes a free 30 GB. You may also select 60 GB, 100 GB, 250 GB, 500 GB, and 1 TB for an additional fee.
