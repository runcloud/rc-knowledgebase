When you click **restore backup**, you need to choose which Web Application / Database backup files will be deployed. You can deploy your backup to any Web Application / Database in any server that you own.

For Web Application, restoring backup files will overwrite your files inside the Web Application if they already exists.
