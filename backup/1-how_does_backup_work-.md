The process of the RunCloud backup is between the client server and the RunCloud backup server. Once RunCloud backups are activated through the dashboard, you can choose to backup web apps, databases, or both.

Our RunCloud agent compresses all of the files in the web app and retrieves the content of the database from the client server. The RunCloud backup server will request to download files from your server.

Once all information is matched up and the servers establish a secure connection, the files or database will be transfered to the RunCloud backup server.

<div class="alert alert-info">
All files and databases are full backups and not incremental.
</div>
