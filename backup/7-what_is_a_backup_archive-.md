If you have deleted a backup instance, any backup files under it will no longer be bound to any backup instance on your server. Thus, it will be archived, as we will never delete your data unless you choose to do so.

You can still restore a backup by uploading it manually or you delete an archive backup if you no longer wish to keep it.
