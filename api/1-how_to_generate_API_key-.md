On the upper most right, click on the cog wheel.

1. Navigate and click the **API Key** on the menu.
2. An API key and API secret will automatically be generated.

You can use this key to connect to RunCloud via API.

You may generate a new API key a few times every hour, but will likely only need it once. Do not give this API key out as it will allow people to use your RunCloud dashboard.

Once you decide you are ready to use the API, select enable under **Enable API Access**.

You may also restrict RunCloud API so that it only responds to your specified IP addresses.
