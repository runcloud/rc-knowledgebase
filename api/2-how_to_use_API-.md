Once you have got your API Key and Secret, you can test the authentication by sending GET request to /ping endpoint

###### Request

```bash
curl -X GET "https://manage.runcloud.io/base-api/ping" \
    -u YOUR_API_KEY:YOUR_API_SECRET \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

If your request is successful, you will get pong message as the output.

###### Response

```json
{
	"message": "pong"
}
```
