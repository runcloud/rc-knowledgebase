#### Fix high usage of memory

On the menu, select Server Health and you will be able to see your resource usage including Server Load, Memory Usage, and Disk Usage. If you see that your resource usage is high or receiving notifications via email that your server is using too many resources, try the following:

1. Restart your server
2. If using WordPress, delete any plugins not in use and make sure the theme are using is properly updated
3. Check the error log
4. Ensure that all of your cronjobs are running successfully
5. Optimize your MySQL database
6. Enable any caching plugins or scripts that may help your website run more efficiently
7. Update your PHP version to the latest (7.3+)
8. Buggy PHP code will cause issues
9. Under Web Application settings, increase the **Memory Limit**
10. Turn off any services not in use
11. Upgrade your VPS plan

If problems persist after trying these trouble shooting tips, contact technical support.
