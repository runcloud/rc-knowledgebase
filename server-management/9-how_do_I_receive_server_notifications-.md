#### Setup server notifications

On the menu, click on **Notification**.

##### Slack

You will need to install the Slack extenstion that enables web hooks. Enter in the URL and select **enable** and **save**.

##### Telegram

Add the RunCloud bot to your Telegram and include your _Authorized Token_.

##### Health Notification Setting

Select how often you would like to receive notifications, including _never_, _6 hours_, _12 hours_, _1 day_, _3 days_, _1 week_, _2 weeks_, _1 month_, and _1 year_.
