On the menu, click on **Settings** and under **Server Details** is the ability to change your server name.

<div class="alert alert-info">
    This does not change the name of your actual server, only how RunCloud will list your server in the dashboard.
</div>
