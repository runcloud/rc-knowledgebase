#### Transfering server to other account

On RunCloud, you may transfer your server from one account to another by clicking on **Settings** and scrolling down to Transfer Server. Enter in the email address that will be taking over this server. Once this transfer is complete, the email address that originally owned the server will no longer have access through RunCloud.

<div class="alert alert-warning">
Transferring a server will remove Slack Webhook URL, Telegram Notification and Health Notification setting. Any backup instances for web applications and databases will be deleted. Any SSL will need to be re-authorized. Once you have requested a transfer, the invitation must be accepted within 15 days, otherwise it will be cancelled.
</div>

#### Accepting a server transfer

To accept a server transfer from another account

1. Navigate to **Setting**.
2. Then click on **Server Transfer**.
3. You will see, the server list that are being transfer to you. Click **Accept** on the action.
