#### Create a Cron Job

A cron job is a task that a server will run. In most cases, it is a script to be run at a designated time. These scripts can be run from as little as every minute per day (1,440 minutes in a day, therefore, it would run 1,440 times per day) or you can run a cron job once per day, once per week, or even once per year.

On the sidebar menu, choose Cron Job. Then click Create Cron Job.

1. Give the cron job a name.
2. Select the system user responsible for this cron job.
3. It is recommended that unless you know what you are doing, you leave the **Vendor Binary** on the default setting of `/bin/bash` which is where it will be called from when running a cron job.
4. Command is where you will add the command you want to run including the script of what you want to run. For example, if you want to create a new log file everyday with some type of information, you would add a script like this:

    ```bash
    * * * * * /runcloud/user/rc-user/myapp/public/scripts/random.php
    ```

    This is a script that tells the computer, "run the random.php file every minute". Since RunCloud does not require you to enter this into the Terminal, you can use the RunCloud dashboard to do the job for you.

5. Predefined Settings is where you would designate how often the script should run. While running it every minute is fine, it really depends on what the script is actually doing. If you are only running a few lines of code every minute, than it is no big deal. However, if you have hundreds to thousands of lines of code running automatically every minute, this may put some demand on your server.
6. The Custom Settings for changing time can be set with Predefined Settings or you can use a tool like [Crontab Guru](https://crontab.guru/) which will allow you to define how often you would like your script to run. For example, if you wanted it to run every 48 hours, it is very possible to do this, and Corntab.Guru will help you figure out how to define those settings.
