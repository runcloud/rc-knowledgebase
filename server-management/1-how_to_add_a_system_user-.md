#### Add system user

Creating a System User is recommended before creating web applications and is highly advantageous for privacy. With every web application, a new system user should be created for maximum security benefits.

1. To create a new system user, navigate the **menu** to **System User**.
2. Click **Create.**
3. Choose a username. Choose a password.
   <br>
   <br>
    <div class="alert alert-success">
    It is highly recommended that you click on Generate Password and let RunCloud generate a 32-character password for you.
    </div>
4. Click on Add User.

And you have created a new system user!
