The server action log records everything you do within the RunCloud dashboard and can be viewed by clicking on Log on the left. The Log is read-only and reports only things that have happened.
