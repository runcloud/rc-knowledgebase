#### Create a Supervisor Job

On the menu, click **Supervisord** and click on Create Supervisord Jobs.

1. Enter in a job name.
2. Select the user who will be given access to run this job.
3. Select whether the job will auto-start and auto-restart.
4. Select the number of processes that will be allowed to run.
5. Select the Vendor Binary, which is the PHP version you are using for your web apps. (i.e. `/RunCloud/Packages/php73rc/bin/php`).
6. Select the Directory of the job (if applicable).
7. Type in any additional commands that will be used to run the job.
8. Click Add supervisord job and the job will run nonstop.
