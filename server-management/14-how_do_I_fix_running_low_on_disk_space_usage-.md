#### Fix running low on disk space usage

You may be running low on disk space.

You can check individual directories and their disk space usage in terminal by typing:

```
sudo du -h --max-depth=1
```

Remove unused Linux packages:

```
sudo apt-get autoremove
```

Clean up the cache:

```
sudo apt-get clean
```

Several more ways to fix this are:​

1. Upgrade your VPS for more space.
2. Use a CDN.
3. You can clear up MySQL logs or alternatively install a WordPress plugin:
    ```
    rm -rf /var/log/mysql/\*
    ```
4. If using WordPress, download a plugin called Smush to compress jpg and png images
5. If using WordPress, remove any unused plugins or themes
   If you are unable to free up additional disk space or feel your web app is using too much without any explanation, feel free to contact technical support.
