Navigate to **Delete Server** on the menu. In order to remove your server completely, you must confirm a total of FOUR (4) times. Afterward, you will click **Delete Server**.

<div class="alert alert-danger">
This action cannot be undone. Once a server has been deleted, it can never be restored. This action only deletes the server from RunCloud and will still need to be removed from your hosting provider.
</div>
