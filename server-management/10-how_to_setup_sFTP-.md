### Introduction

We do not recommend ever using FTP as it is not secure. FTP (File Transfer Protocol) uses clear text for all transmissions. Anyone can read the FTP usernames, passwords, commands and data by packet capture (sniffing) on the network.

By default, [RunCloud Firewall](https://runcloud.io/docs/guide/server-management/security) blocks FTP (port 21) on the server level. Thus, you are recommended to use SFTP for file transfer instead.

<div class="alert alert-success">
    You can bypass sFTP access and use the RunCloud File Manager in the RunCloud panel for simple file management.
</div>

If you need more, you can use sFTP. SSH File Transfer Protocol (SFTP) is a secure file transfer protocol for file access, transfer, and management. It adapts the Secure Shell (SSH) protocol with encryption and secure authentication on both server and client.

SFTP provides two user-authentication options when connecting to your server: a) passwords or b) SSH2 key-based authentication. SSH Keys are more secure than password.

The application we recommend using is Filezilla. FileZilla is a free, open source FTP client that supports FTP, SFTP, FTPS and is available for Windows, Mac OS X, and Linux. [Download FileZilla Client](https://filezilla-project.org/download.php?type=client).

#### SFTP via Passwords

1. Open FileZilla client
2. Open **Site Manger** by clicking the top left icon in menu bar, or press Ctrl+S
3. Click **New Site** button and gives a name to the new site connection
4. At the right side panel enter the following information:
    - **Host**: [RunCloud server IP address]
    - **Port**: 22
    - **Protocol**: SFTP - SSH File Transfer Protocol
    - **Logon** Type: Normal
    - **User**: [System User's username]
    - **Password**: [System User's password]
5. Click **Connect** button to connect (**OK** button to save the connection)
6. Accept fingerprint warning when prompt
7. You are now connected to RunCloud server via SFTP using password

#### SFTP via SSH2 Key-based Authentication

If you have not yet create an SSH key pair, then follow one of the two tutorial below:

-   Windows users : [How To Use SSH Keys with PuTTY on RunCloud](https://blog.runcloud.io/2017/12/28/how-to-use-ssh-keys-with-putty-on-runcloud.html)
-   Mac and Linux users : [Generating SSH Keys](https://runcloud.io/docs/guide/server-management/ssh-key#generating-ssh-key)

##### Method #1

1. Open FileZilla client.
2. Open **Site Manger** by clicking the top left icon in menu bar, or press Ctrl+S
3. Click **New Site** button and gives a name to the new site connection
4. At the right side panel enter the following information:
    - **Host**: [RunCloud server IP address]
    - **Port**: 22
    - **Protocol**: SFTP - SSH File Transfer Protocol
    - **Logon** Type: Key file
    - **User**: [System User's username]
    - **Key** file: [select your SSH _private key_ in .ppk or .pem format]
5. Open FileZilla client
6. Click **Connect** button to connect (**OK** button to save the connection)
7. You are now connected to RunCloud server via SFTP using SSH key

##### Method #2

1. Open FileZilla client
2. In the **Edit - Preferences** menu, select **SFTP** under **Connection**
3. Click **Add key file** button to add your SSH _private key_ (If your key is in OpenSSH format, FileZilla will prompt you to convert it to PuTTY's PPK format)
4. Click **OK** button to save setting and exit Preferences window
5. Follow **Method #1** above but select **Interactive** for **Logon Type**
