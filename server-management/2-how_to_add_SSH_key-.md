#### Add SSH Key

Windows users should please [follow this guide](https://blog.runcloud.io/2017/12/28/how-to-use-ssh-keys-with-putty-on-runcloud.html) to generate their keys.

If you are using an \*NIX operating system (Linux/MacOS/Unix), it is very easy.

1. To generate your SSH, run the `ssh-keygen` command from the terminal on your local machine. Your terminal will confirm generation of the key pair and ask where to save the keys.

    ```bash
    $ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/Users/localuser/.ssh/id_rsa):
    ```

2. Hit **enter** to accept the path and filename.
3. Copy the public SSH key from our local machine.
4. Print out the public key in the terminal using the `cat` command and the `/path/to/your/id_rsa.pub`. Like so:

    ```bash
    $ cat ~/.ssh/id_rsa.pub
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD0NlbKiLwqNHF/H+OcQd6ShGz34874837
    sdkniwkajhsdkjahdssflmckjkaslfZObTqh6vhbNxTChUbrIZpICE9wqTYVIvNDRc75bM2YEXBymYpZ4ZE17WwT+ko2UsxygWC+yxwG9p348973298472lksndakashdCYxdE232n5l1hQe38VPDlEOBcti8ZRYO42XQxv0jcb86jbpbNoqO2yCfa/Aj6hJaSLcbGVdZRAijU/JCcKd7WTGIIhGWrw43q+HDLuF+9Z local_user@computer.local
    ```

5. Copy this key to the clipboard.

#### Add the public SSH Key

1. Navigate to **SSH Keys** in the RunCloud panel for your servers, and click Create **SSH Key**:
2. Click the button to add an SSH key for a System User.
3. In the **Add SSH Key** panel, add a label for the Key, choose the **System User** to assign the key to, and paste the Public SSH Key from the clipboard into the **Public Key** box.
4. Add your Public SSH Key to your System User.

A list of all your SSH Keys, labelled and assigned to System Users.
