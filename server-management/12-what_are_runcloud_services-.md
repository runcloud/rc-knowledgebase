#### Introduction

When the RunCloud software installs, it includes additional packages that are known as services, which directly affect the server. By default, most of the services are disabled and you must enable them manually. Some of these services may easily be self-explanatory while others may leave you guessing as to what they actually do.
If you aren't sure what they are or what they do, continue reading because we're going to cover what each of these do more in-depth!

##### NGINX

Nginx is an open source frontend web and proxy server with a focus on high performance, high concurrency, and low memory usage. Nginx can help speed up websites by caching content, serving SSL/TLS, and acting as a load balancer, utilizing asychronous functioning to perform tasks. It is the primary web server that your website will run on when using RunCloud.

You may set up a website using Nginx by itself without Apache. This method is recommended if you have mostly static content.

##### HTTPD/APACHE

Apache is a backend web proxy server that is fast and secure. It is highly customizable with many extensions and modules available including gzip, caching, and security features. Apache basicaly handles the communication requests between browser and server as well as the processor for server-side programming languages.
PHP is served by Apache using FastCGI/FPM.

RunCloud allows you to use Nginx + Apache together. When combined, there is a significant powerful effect: Nginx serves as a static web server frontend while Apache is processing in the backend.

The combination often allows your website to load in under 2-3 seconds.

##### MariaDB

MySQL and MariaDB are very similar. Many webhosts prefer MySQL over any other database and while MySQL is optimal for most websites, specifically WordPress, MariaDB is faster and more secure. MariaDB also supports virual columns, which make database calculations far more efficient, as well as its caching feature.

The whole reason for using RunCloud is for the automatic updates to security features on your server. This does include automatic upgrades of MariaDB whenever there are any security patches or new releases.

##### Supervisord

You will not find anything like Supervisord on any other server management tool. Supervisor allows its users to monitor and control a number of processes on the server. Supevisord will run many different types of scripts including NodeJS. Unlike a crontab, Supervisord is running 24/7/365/1440/86400 (hours/weeks/days/minutes/seconds) and never stops while maintaining its utmost important task: not using up or a lot of your server resources.

##### Redis

A superior choice when it comes to server-level database caching without actually using a database. Redis is a data structure server. Redis helps caches the most common types of queries and hits to the database, reducing server load, and speeding up up your website. As Redis learns the the requests of your server, it automatically picks up any data that has been cached, making communication between server and database and database and user much quicker. It improves server memory and reduces the usage of server resources.

<div class="alert alert-info">
    Redis may require additional configuration to run in WordPress and other CMS.
</div>

##### Memcached

While Redis is preferred over Memcached, this database server-level caching system is much older and more utilized than its counterpart. Redis + Memcached is highly recommended though getting the two to work together is not always easy. RunCloud has automatically configured both of these turn them on, and you are set to go. It may take at least a dozen visits to your website to notice the cache system working, ultimately, memcahced does a great job of also caching the most common queries, and helping to speed up the server. Redis + Memcached is a way to optimally run your server and keep it fast.

##### Beanstalkd

Beanstalkd is used as a work queue, in which scripts are added to a job queue and run in the background, rather than actively. The work queue within Beanstalkd can handle tasks such as adding to the database, counting files, sending emails, etc. Enabling this will not make it do anything on its own, as you will have to write your script add and get data from Beanstalkd. For more information on Beanstalkd, read: Working with PHP and Beanstalkd.

All of these features may be turned on or off with the click of your mouse in RunCloud and are quite useful to help speed up and perform tasks on your server. Give RunCloud a try today and turn on all of these services as once for optimized performance!

#### Starting and stopping services

All services on RunCloud can be started or stopped and not all services are necessary to keep running. To keep them running may use up resources that may be needed elsewhere. By default, only services being used are kept enabled. To start or stop them, simply hover over and click on them.
