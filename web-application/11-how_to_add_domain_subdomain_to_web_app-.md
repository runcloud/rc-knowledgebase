On the **Web Application** screen:

If you are creating a web application, enter in your domain or subdomain under **Domain name**.

If you are changing or adding the domain or subdomain after adding your web application, click on the name of your web app. On the menu, select **Domain name**. Enter in the domain name and click **Attach Domain Name**.

It may take several moments for it to render through DNS.
