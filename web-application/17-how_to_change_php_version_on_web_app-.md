On the **Web application** page, choose **Settings** from the menu.

Scroll down to _PHP Version_ and select the desired PHP version.

<div class="alert alert-warning">
    Due to security reasons, we do not support PHP before version 7.x.
</div>
