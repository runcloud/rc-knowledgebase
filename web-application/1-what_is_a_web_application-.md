A web application is another term for a website.

Each web app is separate from each other. Most people who come from cPanel tend to put everything inside a folder and then serve it to the public. Similarly, each web app is separated into its own folder, with its own database and permissions, sandboxed, and served to the public.

Sandboxing each web app allows it to be secure and protected in its own right. If one of your websites is compromised, the others will be completely safe.