On the **Web application** page, choose **SSL/TLS** from the menu.

Scroll down and choose Custom.

Enter in your **Private Key** and the **Certificate**.

Click Submit. It may take several moments for your website to render as https.
