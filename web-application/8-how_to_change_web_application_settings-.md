This section contains two section which is **PHP Version selection** and **Web Server Setting**.

#### PHP version

Here you can change your Web Application PHP version with just one click.

#### Web server configuration

The web server configuration consists of several section:

##### Public path

This is where you can change your web application public path. For example if you are using PHP framework, you can change your public path to `/path/to/framework/public`

Web application stack
There are two stack that you can choose from here.

1. NGINX + Apache2 Hybrid Stack:
   Nginx works as reverse proxy for Apache2. Different from most setups, this stack doesn't fully proxy Apache2. Nginx is really best at serving static content. By using this stack, your static files (eg: css,js,images,fonts) will be served by Nginx. If it is PHP, Nginx will pass it to Apache2 and the request will be pass to PHP-FPM. This setup is great if you are leveraging with .htaccess.

2. Native NGINX Stack:
   Your sites will be fully controlled by Nginx. If it is PHP files, Nginx will pass it to FastCGI to communicate with PHP-FPM. This stack doesn't support .htaccess. If you are going to rewrite or including your own config, you can extend Nginx config by following this example.

3. Native NGINX + Custom config Stack:
   Your sites will be fully controlled by Nginx. This stack will not serve your PHP file. You have to manually extend the config by following this example. This is suitable if you want to run nodejs/python/golang/websocket/ror/etc.

Web application stack mode
There are two stack mode that you can choose from here.

1. Production
   This will instruct visitor to your web application to cache static files (js/css/fonts/images/html) inside their browser.

2. Development
   This will prevent the browser from caching your static files (js/css/fonts/images/html). Development mode does not prevent from other caching method such as Redis, Memcached or in-app caching.

##### NGiNX

This is the configuration for your Nginx block for the Web Application. Changing this value will have no effect on other Web Application that you own

Checkbox Reference Link

-   **Clickjacking Protection :** [Learn More](https://en.wikipedia.org/wiki/Clickjacking)
-   **XSS Protection :** [Learn More](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection)
-   **MIME Sniffing Protection :** [Learn More](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options)

##### FPM

This is the configuration for PHP-FPM settings for this Web Application. It will have no effect on other Web Application.

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Process Manager</td>
            <td>How the PHP-FPM run. There are <strong>three</strong> pm settings that you can choose which is <i>ondemand</i>, <i>dynamic</i> and <i>static</i>. Our default settings use <i>ondemand</i> because it will scale depending on number of visitors you have on your website. Read <a href="https://ma.ttias.be/a-better-way-to-run-php-fpm/" target="_blank" rel="nofollow">here</a> for more info</td>
        </tr>
        <tr>
            <td>pm.start_servers</td>
            <td>You can only set this if pm is set to <i>dynamic</i>. When you start the FPM process, it will start with this number of process</td>
        </tr>
        <tr>
            <td>pm.min_spare_servers</td>
            <td>The desired minimum number of idle FPM processes. Used only when pm is set to <i>dynamic</i></td>
        </tr>
        <tr>
            <td>pm.max_spare_servers</td>
            <td>The desired maximum number of idle FPM processes. Used only when pm is set to <i>dynamic</i></td>
        </tr>
        <tr>
            <td>pm.max_children</td>
            <td>Allowed number of process for PHP-FPM to fork</td>
        </tr>
        <tr>
            <td>pm.max_request</td>
            <td>The number of request can each PHP-FPM child serves</td>
        </tr>
    </tbody>
</table>

##### PHP

This is the PHP configuration for your Web Application. It will have no effect on other Web Application. It will also have no effect on <a role="button" href="/docs/guide/server-management/php-cli">PHP-CLI</a>.

<table class="table">
    <thead>
        <tr>
            <th>Form Field</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>open_basedir</td>
            <td>The maximum parent folder that the web application can execute. PHP won't be executing outside of this folder. If you have to change this setting, there is something unusual going on inside your web application. It is safe to say that you code your web application wrongly</td>
        </tr>
        <tr>
            <td>data.timezone</td>
            <td>The timezone that will be used inside your web application. If you have set your timezone inside the profile, this value will follow your current timezone.</td>
        </tr>
        <tr>
            <td>disable_function</td>
            <td>PHP function that should be disable for security reason. If you need to use one or more of this php function, do not blindly remove all function. You might pose high security vulnerability if you remove everything from here.</td>
        </tr>
        <tr>
            <td>max_execution_time</td>
            <td>The maximum number of seconds that PHP script can execute. PHP script will stop itself after reaching this time</td>
        </tr>
        <tr>
            <td>max_input_time</td>
            <td>The maximum number of seconds that a script allows to receive input from website visitor such as uploading picture. If your website host video uploading site, you might need to increase this value</td>
        </tr>
        <tr>
            <td>max_input_vars</td>
            <td>The maximum number of variables can a request handle.</td>
        </tr>
        <tr>
            <td>memory_limit</td>
            <td>The maximimum amount of memory (in MB) that a script allowed to use</td>
        </tr>
        <tr>
            <td>post_max_size</td>
            <td>The maximum size of post request (in MB) your server can handle.</td>
        </tr>
        <tr>
            <td>upload_max_filesize</td>
            <td>The maximum size of uploaded file your Web Application can handle. Usually same or lower than post_max_size.</td>
        </tr>
        <tr>
            <td>sesion.gc_maxlifetime</td>
            <td>The maximum number of seconds your PHP session can be valid before garbage collector delete your session.</td>
        </tr>
        <tr>
            <td>allow_url_fopen</td>
            <td>Allow your php script to open remote files from another server. The default value is on, but if you don't use it, you may disable this setting.</td>
        </tr>
    </tbody>
</table>
