Below is a summary of the full data of the web application section.

<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Justification</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Owner</td>
            <td>The username of the user who owns the web application.</td>
        </tr>
        <tr>
            <td>Total Domain Name</td>
            <td>Total domains connected to this web application.</td>
        </tr>
        <tr>
            <td>PHP Version</td>
            <td>PHP version of this web application.</td>
        </tr>
        <tr>
            <td>Web Application Stack</td>
            <td>Stack that you use. Either NGINX + Apache2 Hybrid, Native NGINX, Native Nginx + Custom config stack.</td>
        </tr>
        <tr>
            <td>Root Path</td>
            <td>The root folder of your Web Application. PHP can be executed inside this directory only.</td>
        </tr>
        <tr>
            <td>Public Path</td>
            <td>Path to the public folder that your visitors can access.</td>
        </tr>
        <tr>
            <td>Total data transfer yesterday</td>
            <td>Total amount of data that your website served yesterday.</td>
        </tr>
        <tr>
            <td>Total data transfer this month</td>
            <td>Total amount of data that your website served this month.</td>
        </tr>
        <tr>
            <td>SSL Provider</td>
            <td>Let's Encypt or custom SSL provider</td>
        </tr>
        <tr>
            <td>GIT Provider</td>
            <td>The name of GIT repository provider that you are using</td>
        </tr>
        <tr>
            <td>Repository</td>
            <td>GIT repository that you are using</td>
        </tr>
        <tr>
            <td>Repository URL</td>
            <td>URL to your GIT repository from your GIT provider</td>
        </tr>
        <tr>
            <td>Branch</td>
            <td>Current GIT Branch that you are using</td>
        </tr>
    </tbody>
</table>
