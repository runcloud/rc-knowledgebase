On the **Web application** page, choose **Settings** from the menu.

Scroll down to `MEMORY_LIMIT` and change the numbers.

We recommend 256 MB but your settings can be as much as your server will allow.
