On the web application screen, under the Options section, click the cog wheel, and select **Delete**.

You must type in the web application name that you want to delete and click **Yes, Delete it**.
