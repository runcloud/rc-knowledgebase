#### Create a database

1. Navigate to **Database** on the left sidebar.
2. Click on **Create Database**. Choose a name for your database and the collation (optional).
3. Lastly, click **Add Database**.

<div class="alert alert-warning">
    You'll need to assign a user to database get this thing working.
</div>

To create a system user to attach to a database, scroll down before clicking **Attach User +**.

#### Create a database user

1. Enter in the username you wish to use. It should actually be something that is not too easy to guess, meaning you should add a capital letter, followed by some lowercases, a number and a symbol.
2. Use our password generator to ensure your absolute and guaranteed protection with the recommendation of a 32 random character password. You can set this lower or higher if you prefer.
   3.Afterward, click Add Database User.
3. Earlier, you had to scroll down from the Attach User + button, but now you can click it and assign the user to the database.
   Adding mydbuser1 as the user to the database

Once added, the database will show a primary user attached to it.
