You can set your web application as default web application. If you set your web application as default, every DNS result that is pointing to your server will go to this web application if there is no matching app name inside your web app. If a visitor tries to access your server by using the IP Address, this web app will be served to the user.

To do this,

1. Click on **Web application** on the menu.
2. Locate the web app you to make the default, click on the cog wheel.
3. Select **Set as default Web Application**.
