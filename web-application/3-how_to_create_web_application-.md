#### Introduction

Once you navigate back to the RunCloud server, you will want to add an application by clicking on **Web Application** on the menu.

1. On this page, you will click **Create Application**.
2. On the next screen fill out all the details.
   The domain can be a nonexistent one for now, and it is always best to use the latest version of PHP, unless you need an earlier version for backwards compatibility reasons.
3. Click **Add Web Application _firstapp_**.
   Your app will be created pretty quickly and you will receive the details for your newly created web app.
4. Click on **More** and **Set as default Web Application**.
5. Click on **Set As Default**.
6. The next step is to determine whether you want to create your website from scratch or use the Script Installer.
7. Two option to create your website.

#### Create your website from scratch

1. Click on **File Manager**, clicking on **New File** and creating your HTML page, defined as **index.php**.
2. Click on create and the new file will now appear in the directory. Click on it and then **View/Edit**. If you decide to edit the file, RunCloud comes with a File Editor tool. Your next page will look like an HTML editor.
3. Add this HTML and hit ctrl+s or click the **save** link above the editor.
   `<html><body><head><title>My First Webpage</title></head><body>Hello World!</body</html>`

Congratulations on your first web app!

#### Create your website using script installer

If you want to use the Script Installer feature, the dropdown menu includes:

-   Concrete5
-   Drupal
-   Grav Core
-   Joomla
-   MyBBA
-   phpBB
-   phpMyAdmin
-   Matomo
-   Prestashop
-   WordPress

1. Select the desired application and click **Install**.
2. Now revisit your web application and you will be prompted with what to do next, usually installing the software.
3. Before you proceed, you will likely need to ensure you have a database and user set up in order to complete the installation process.
