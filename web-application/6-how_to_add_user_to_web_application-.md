On the **Web application** screen, after clicking on _Create Web App_.

Locate _User (owner of this web application)_ and select the user.

If you prefer to create a new user, navigate to **System user** on the menu and click **Create**.

Return back to the **Web application** screen and add the new user.
