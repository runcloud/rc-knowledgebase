One of the best features of RunCloud is that you can use GIT to attach your project to your Web Application. You can do an automatic pull to sync your website with your code. You can also change your GIT branch on the fly. If you are using GIT, you won't be able to use Script Installer inside the Web Application.

1. On the **Web Application** screen, click on **Git**. To use this function, you must place your Deployment Key into your GIT repository. You can get your deployment key in Deployment Key section.
2. Select a provider: Custom Git Server, Bitbucket, Github, GitLab, or Self Hosted GitLab.
3. Enter in your Github username and the repository name.
4. The branch, by default, should be Master, but change it if necessary.
5. Click on **Attach GIT Respository** to attach Git to your web application..
