#### Troubleshooting

1. Ensure you are applying SSL to the correct domain.

2. Check that all domains have an accurate A record pointing to the Server IP or through Cloudflare.

    Once you have pointed the A record to the correct domain, you may proceed with SSL installation

3. Check if the nginx-rc server up.

    If the Nginx is down, SSL will not be installed. Ensure it is started before applying SSL.

4. Check the logs of web application.

    The logs will often provide the error results for why SSL is not being installed

5. Check if AAAA records are active and disable them before proceeding further with the SSL installation.

6. Check the permission of /opt where the LetEncrypt authorization file is there.

    LE authorization file to check: `/opt/RunCloud/letsencrypt/.well-known/acme-challenge/`

7. Wait patiently and clear cache as SSL may take up to 24 hours render.

8. Disable and re-enable SSL.

9. If all of the above doesn't work, please check that port 80 is working.

10. Finally, as a last resort, contact Technical Support to look into the issue.
