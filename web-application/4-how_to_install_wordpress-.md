#### Install WordPress using the script installer

To access the script installer menu, navigate to the Script Installer tab in the Web Application menu and choose WordPress from the dropdown menu. Click on the Install button to install WordPress as your web application.

Once you've clicked installed, RunCloud will let you know when the process is complete.

If you are not using any domain name, you can access the website via the IP address, but to do this, you need to set the web application as the default web application by navigating to the Web Application section and clicking on the cogwheel and clicking Set as default Web Application.

You can visit the website via the IP address or domain once you have routed it through the DNS.

#### Setting up your database

Before you can complete the WordPress installation, you need to setup the database.

1. Head over to the Database section and click the **Create Database** button.
2. Add a Database user and Database password.

It is recommended that you use the **Generate Password** feature.

Complete the required details and click **Save Database** to create the database. Since you cannot see the password after the database has been created, it is advisable to copy the password and keep it in a safe place for future use. Once you add it into WordPress for installation, it will also be stored in the wp-config.php.

#### WordPress installation and configuration

1. Choose your desired language and click **Continue**.
2. Click **Let's Go!**
3. Fill in the required database fields and click **Submit**.

If WordPress is able to connect to your database, you will see a screen for you to fill out your website information. Once completed, click on Install WordPress, and you will be informed about the success of the installation. Once done, you can navigate to your new website.

Congratulations! You have successfully installed WordPress using RunCloud.
