On the **Web application** page, choose **SSL/TLS** from the menu.

Scroll down and choose the Let's Encrypt enviornment to **Staging** if you are still testing or **Live** if your website is in production.

Click Submit. It may take several moments for your website to render as https.
