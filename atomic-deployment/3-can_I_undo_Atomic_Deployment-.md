Atomic Deployment cannot be undone and the GIT repository would need to be deleted from the web application to remove Atomic Deployment.

<div class="alert alert-danger">
You should only use Atomic Deployment if you know what you are doing. If you have any additional questions, do not hesitate to contact technical support.
</div>
