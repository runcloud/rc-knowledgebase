#### Adding rules to your firewall

Depending on the web host you are using, you will need to add firewall rules to your server to allow RunCloud to install without interruption.

By default, RunCloud will close all unused ports once it is installed to ensure as much security as possible, however, it will need certain ports to be opened.

Ensure that you have opened up ports:

-   **SSH:** 22/tcp
-   **HTTP:** 80/tcp
-   **HTTPS:** 443/tcp
-   **RunCloud Communication Port:** 34210/tcp

RunClud uses FirewallD as the primary firewall and Fail2Ban to block unauthorized attempts to access your server.
