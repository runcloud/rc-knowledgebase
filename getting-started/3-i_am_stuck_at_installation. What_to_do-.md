#### Stuck at 100% installation

If you are stuck at 100%, here are a few things you can try:

1. Ensure that your VPS was fully installed and initiated before installing the RunCloud software.
2. A fresh instance of a VPS is necessary, as RunCloud cannot be installed on any pre-existing software.
3. Your VPS instance must be Ubuntu 16.04/18.04.
4. Ensure that your firewall is allowing ports 22, 80, 443, and 34210.
