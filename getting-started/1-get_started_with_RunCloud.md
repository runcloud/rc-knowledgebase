#### Introduction

Looking for great automated server management? [RunCloud](https://runcloud.io/) is a step towards autonomy and control over your own servers, giving you access to install multiple websites, easy database setup, enhanced security measures, and automatic updates.

RunCloud serves as a control panel that offers one-click solutions to common tasks including web deployment with Git, script installation to install common Content Management Systems, and provides the most optimized server stack including Nginx, Apache, Redis, MariaDB, Memcached, and more.

#### Step 1: Sign up with a VPS

RunCloud works with any VPS so you are not limited to our choosing.

#### Step 2: Deploy a new server

To get started deploying your server, log into the control panel of your VPS and Deploy a server.

1. Select a hostname and description for your server
2. Select the location of your server
3. Select the size of your server
4. Select Ubuntu 18.04 as your server's operating system
5. Deploy!

#### Step 3: Sign up with RunCloud

Register for an account on [RunCloud](https://runcloud.io/). Click SIGN UP on the home page to begin. Enter your credentials to create an account. Once done, you will need to verify your email address and then you may login.

#### Step 4: Connect a server to RunCloud

Earlier, you signed up for an account on RunCloud and were taken to a screen where we left off and did not proceed further. Switch over to RunCloud or sign in and you will see a box that looks like this:

<div class="alert alert-info">
    Before installing RunCloud, please ensure your VPS is compatible.
</div>

Anything other than following these installation notes will result in errors.

Click on **Connect a New Server**.

1. Enter in the name of the server, which can be any name you want to give it.
2. Enter the IP address of the server.
3. Enter in a Server Provider, which is optional, such as where the server actually is.
4. Click Connect this server.

You will be given a script on the next screen which you will need to copy into your clipboard.

<div class="alert alert-info">
In April 2019, RunCloud will no longer require this script to be copied and pasted into Terminal.
</div>

You can use SSH via Mac OS Terminal, Linux Terminal, or use WinSCP for SSH. If you are confused about that language, download this Chrome extension you can download called Secure Shell App. You will be logging in by typing in command below and hit enter.

```bash
root@ipaddress [change ipaddress to the actual IP address]
```

You will be prompted with a "fingerprint", for which you will want to answer **Yes**. For the password, you will copy from your VPS provider screen and paste it right into the terminal. It will not display the actual password for security reasons.

Once you've passed this, you will see a command prompt. Copy the code provided by RunCloud. Paste this code into Terminal and hit enter.

The installation process will begin and takes about 10 minutes. You will see status updates as everything downloads and installs automatically. When everything is finished, you will get a screen with a username and two passwords. Make sure you copy that information and store it somewhere safe on your computer.
